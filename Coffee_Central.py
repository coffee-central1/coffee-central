import numpy as np
class City:
    def __init__(self,dx,dy,q,n):
        self.dx = dx
        self.dy = dy
        self.q=q
        self.n=n
        self.City = np.ndarray(shape=(dx,dy),dtype=int)
        self.City.fill(0)
        
    def load(self,x,y,q):
        u = self.dx
        v = self.dy
        low_x = max(0,x-q)
        high_x = min(u-1,x+q)
        for i in range(low_x,high_x+1):
            dist = q - abs(x-i)
            low_y = max(0,y-dist)
            high_y = min(v-1,y+dist)
            for j in range(low_y,high_y+1):
                self.City[i][j] += 1
                
    def return_best(self):
        max_coff= 0
        x_loc = 0
        y_loc = 0
        for i in range(dx):
            for j in range(dy):
                if max_coff<city.City[i][j] or (max_coff == city.City[i][j] and y_loc>j) or (max_coff == city.City[i][j] and y_loc==j and x_loc<i):
                    max_coff = city.City[i][j]
                    x_loc = i+1
                    y_loc = j+1
        return max_coff,(x_loc,y_loc)
    
    
a = list(map(int,input().split(" ")))
while min(a)!=0:
    dx = a[0]
    dy = a[1]
    n = a[2]
    q = a[3]
    city = City(dx,dy,q,n)
    locations = []    
    for i in range(n):
        t = list(map(int,input().split(" ")))
        locations.append(t)
    for l in range(1,q+1):
        query = int(input())
        for k in range(0,n):
            city.load(locations[k][0]-1,locations[k][1]-1,query)
        print(city.return_best())   
       
        city = City(dx,dy,q,n)
    
    a = list(map(int,input().split(" ")))
